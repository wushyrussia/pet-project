package org.wushyrussia.diary.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * @author Wushyrussia
 * Single part of entry from diary
 * created 2021.04.12
 * */

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class EntryPart extends BaseEntity{
    /** Body of this part */
    private String text;

    /** The sequence of parts in a entry body*/
    @Column(nullable = false)
    private int seq;
}
