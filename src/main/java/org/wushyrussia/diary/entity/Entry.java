package org.wushyrussia.diary.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;


/**
 * @author Wushyrussia
 * Single entry from diary
 * created 2021.04.12
 * */

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Entry extends BaseEntity{
    /** Name of entry */
    private String name;

    /** The entry body consist of several parts */
    @OneToMany(cascade = CascadeType.ALL)
    List<EntryPart> entryParts;
}
